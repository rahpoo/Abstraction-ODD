<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 5/2/2018
 * Time: 12:21 PM
 */

namespace app\shapes;

use app\base\BaseShape;

/**
 * Class Rectangle
 * @package app\shapes
 */
class Rectangle extends BaseShape {

    /**
     * @var string
     */
    protected $name = "Rectangle";

    /**
     * @var float
     */
    protected $width;
    /**
     * @var float
     */
    protected $height;

    /**
     * Rectangle constructor.
     * @param float $width
     * @param float $height
     */
    public function __construct(float $width, float $height)
    {
        $this->width  = $width;
        $this->height = $height;
    }

    /**
     * @return string
     */
    public function output()
    {
        return "width={$this->width} height={$this->height}";
    }

}