<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 5/2/2018
 * Time: 1:57 PM
 */

namespace app\base;


//use app\traits\Configurable;

/**
 * Class Object
 *
 * Add some features to object of classes
 *
 * @package app\base
 */
class Object
{
//    use Configurable;

    protected $values = [];

    /**
     * Get fully name of this class
     *
     * @return string
     */
    public static function className()
    {
        return get_called_class();
    }

    // If using Configurable then this constructor should be active
    // by using this feature component can call by passing parameters as one array
    // Example: ['width' => 200, 'height' => 200]
    /*public function __construct($config = [])
    {
        if(is_array($config))
            $this->configure($this, $config);
    }*/

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $setter = "set".strtoupper($name);
        if(method_exists($this, $setter)) {
            $this->$setter($value);
        } else {
            $this->values[$name] = $value;
        }
    }

    /**
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        $getter = "get".strtoupper($name);
        if(method_exists($this, $getter)) {
            $this->$getter($name);
        } else {
            if(isset($this->values[$name])) {
                return $this->values[$name];
            }
            else {
                throw new \Exception("$name Property is not exists!");
            }
        }
    }

}