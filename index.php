<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 5/2/2018
 * Time: 12:17 PM
 */

// Define required constants
define('BASE_PATH', realpath(dirname(__FILE__)));
define('DS', DIRECTORY_SEPARATOR);

require_once(BASE_PATH .DS. 'app' .DS. 'Bootstrap.php');