<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 5/2/2018
 * Time: 3:06 PM
 */

namespace app\base;

use app\contracts\iRenderer;
use app\contracts\iShape;
use app\contracts\iWidget;

/**
 * Class WidgetFactory
 *
 * This is a tool for drawing new shapes in runtime
 *
 * @param iWidget $widget
 * @param iShape $shape
 * @param iRenderer|null $renderer [default = CliRenderer]
 *
 * @package app\base
 *
 *
 * @example
 *
 *  $widget = new app\base\WidgetFactory(
 *      new \app\base\BaseWidget(50, 50),
 *      new \app\shapes\Circle(300),
 *      new \app\base\renderer\CliRenderer()
 *  )->draw();
 */
class WidgetFactory
{
    /**
     * @var iWidget
     */
    protected $widget;

    /**
     * @var iShape
     */
    protected $shape;

    /**
     * WidgetFactory constructor.
     * Creating a new shape widget and set a custom renderer for that
     *
     * @param iWidget $widget
     * @param iShape $shape
     * @param iRenderer|null $renderer [default = CliRenderer]
     *
     * @return void
     */
    public function __construct(iWidget $widget, iShape $shape, iRenderer $renderer = null)
    {
        $this->widget = $widget;
        $this->shape  = $shape;

        $this->widget->setRenderer($renderer);
    }

    /**
     * @return iWidget
     */
    public function getWidget()
    {
        return $this->widget;
    }

    /**
     * @return iShape
     */
    public function getShape()
    {
        return $this->shape;
    }

    /**
     * Render to draw result by renderer of widget
     *
     * @return void
     */
    public function draw()
    {
        $this->widget->render( $this->widget->draw($this->shape) );
    }

}