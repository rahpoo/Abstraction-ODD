<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 5/2/2018
 * Time: 12:25 PM
 */

namespace app\contracts;

interface iRenderer {

    public function render($input);

}