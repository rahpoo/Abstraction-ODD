<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 5/3/2018
 * Time: 6:39 PM
 */
namespace tests;

use PHPUnit\Framework\TestCase;

class CanvasTest extends TestCase
{

    public function testCanvasDrawingSimpleText()
    {
        ob_start();

        (new \app\widgets\CanvasWidget())->draw("
-----------------------------------------------------------------------------------------------
Current Drawing
-----------------------------------------------------------------------------------------------\n
");

        $drawing = ob_get_contents();
        ob_end_clean();

        $expected = "
-----------------------------------------------------------------------------------------------
Current Drawing
-----------------------------------------------------------------------------------------------\n
";

        $this->assertEquals( $expected, $drawing, 'Canvas drawer is not work correctly!');
    }

    public function testCanvasDrawingShapes()
    {
        ob_start();

        $canvas = (new \app\widgets\CanvasWidget());

        $canvas->position(10, 10)->add( new \app\shapes\Rectangle(30, 40) )
            ->position(15, 30)->add(new \app\shapes\Square(35))
            ->position(100, 100)->add(new \app\shapes\Ellipse(300, 200))
            ->position(1, 1)->add(new \app\shapes\Circle(300))
            ->position(5, 5)->add(new \app\shapes\Textbox(200, 100, "sample text"))
            ->draw();

        $drawing = ob_get_contents();
        ob_end_clean();

        $expected = "Rectangle (10,10) width=30 height=40\nSquare (15,30) size=35\nEllipse (100,100) diameterH = 300 diameterV = 200\nCircle (1,1) size=300\nTextbox (5,5) width=200 height=100 Text=\"sample text\"\n";

        $this->assertEquals( $expected, $drawing, 'Canvas drawer is not work correctly!');
    }

}
