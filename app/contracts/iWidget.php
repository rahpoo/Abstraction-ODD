<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 5/2/2018
 * Time: 1:46 PM
 */

namespace app\contracts;

/**
 * Interface iWidget
 *
 * @package app\contracts
 */
interface iWidget
{
    public function getX();
    public function getY();

    public function draw($input, $flush=true);
    public function setRenderer(iRenderer $renderer);
    public function render($input);
    public function flush();

}