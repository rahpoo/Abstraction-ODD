<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 5/2/2018
 * Time: 12:20 PM
 */

namespace app\base\renderer;

/**
 * Class CliRenderer
 *
 * It is a simple content renderer in console.
 * Renderer using by widgets
 *
 * @package app\base\renderer
 */
class CliRenderer implements \app\contracts\iRenderer {

    /**
     * @param string $input
     */
    public function render($input)
    {
        if(is_string($input))
            echo $input . "\n";
    }
    
}