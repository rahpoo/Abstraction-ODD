<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 5/2/2018
 * Time: 2:08 PM
 */

namespace app\widgets;


use app\base\BaseWidget;
use app\base\WidgetFactory;

/**
 * Class CanvasWidget
 *
 * This widget is a canvas page to drawing shapes or writing text
 * such as a group drawer of multi widgets
 *
 * @package app\widgets
 *
 *
 * @example
 *
 *  $canvas = (new \app\widgets\CanvasWidget())->draw("
 *  -----------------------------------------------------------------------------------------------
 *  Current Drawing
 *  -----------------------------------------------------------------------------------------------\n
 *  ");
 *
 *  $canvas->position(10, 10)->add( new \app\shapes\Rectangle(30, 40) )
 *          ->position(15, 30)->add(new \app\shapes\Square(35))
 *          ->position(100, 100)->add(new \app\shapes\Ellipse(300, 200))
 *          ->position(1, 1)->add(new \app\shapes\Circle(300))
 *          ->position(5, 5)->add(new \app\shapes\Textbox(200, 100, "sample text"))
 *
 *          ->draw();
 *
 *  $canvas->draw("
 *  -----------------------------------------------------------------------------------------------
 *  ");
 */
class CanvasWidget extends BaseWidget
{

    /**
     * @var array of \app\contracts\iWidget
     */
    protected $widgets = [];

    /**
     * Add a shape or widget to canvas
     *
     * @param $element
     *
     * @return $this
     */
    public function add($element)
    {
        if( is_a($element, '\app\contracts\iWidget') ) {
            $this->widgets[] = $element;
        }

        elseif( is_a($element, '\app\contracts\iShape') ) {
            $this->widgets[] = new WidgetFactory( new BaseWidget($this->getX(), $this->getY()), $element, $this->getRenderer());
        }

        return $this;
    } // End add method

    /**
     * Draw widget(s) or string that added to this canvas
     *
     * @param null|string $per_content that draw before drawing other objects
     * @param bool $flush
     *
     * @return $this
     */
    public function draw($per_content = null, $flush = true)
    {
        if(is_string($per_content) && !empty($per_content)) {
            echo $per_content;
        }

        if(count($this->widgets) > 0) {

            foreach($this->widgets as $widget) {
                $widget->draw();
            } // End foreach $this->widgets

        } // End if !empty $this->widgets

        if($flush === true)
            $this->flush();

        return $this;
    } // End output method

    /**
     * @inheritdoc
     */
    public function flush()
    {
        $this->widgets = [];
    }
}