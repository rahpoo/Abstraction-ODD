<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 5/2/2018
 * Time: 12:23 PM
 */

namespace app\shapes;

use app\base\BaseShape;

/**
 * Class Ellipse
 * @package app\shapes
 */
class Ellipse extends BaseShape {

    /**
     * @var string
     */
    protected $name = 'Ellipse';

    /**
     * @var float
     */
    protected $horizontal_diameter;
    /**
     * @var float
     */
    protected $vertical_diameter;

    /**
     * Ellipse constructor.
     *
     * @param float $diameter_h
     * @param float $diameter_v
     */
    public function __construct(float $diameter_h, float $diameter_v)
    {
        $this->horizontal_diameter  = $diameter_h;
        $this->vertical_diameter    = $diameter_v;
    }

    /**
     * @return string
     */
    public function output()
    {
        return "diameterH = {$this->horizontal_diameter} diameterV = {$this->vertical_diameter}";
    }

}