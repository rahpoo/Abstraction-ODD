<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 5/2/2018
 * Time: 4:31 PM
 */

namespace app\base;

use app\contracts\iShape;

abstract class BaseShape extends Object implements iShape
{
    /**
     * @var string name of shape, [Optional]
     */
    protected $name = '';

    /**
     * Get shape name
     *
     * If name of shape is empty will be filled using class name
     *
     * @return string
     */
    public function getName()
    {
        if(empty($this->name))
            $this->name = static::className();

        return $this->name;
    }

    abstract public function output();
}