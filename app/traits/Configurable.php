<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 5/2/2018
 * Time: 2:37 PM
 */

namespace app\traits;


trait Configurable
{

    /**
     * Set property of an object using config array
     *
     * If using Configurable then this constructor should be active
     * by using this feature component can call by passing parameters as one array
     *
     * @Example: ['width' => 200, 'height' => 200]
     *
     * @param object $object
     * @param mixed $properties
     *
     * @return object
     */
    public static function configure($object, $properties)
    {
        if(is_array($properties)) {
            foreach ($properties as $name => $value) {
                $object->$name = $value;
            }
        }// end if $properties is an array
        else {
            $object->$name = $properties;
        }

        return $object;
    }

    /**
     * Get all public property of an object
     *
     * @param object $object
     *
     * @return array
     */
    public function getObjectVars(object $object) : array
    {
        return get_object_vars($object);
    }

}