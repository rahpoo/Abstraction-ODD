<?php
/**
 * Created by PhpStorm.
 *
 * This is a bootstrap to running app
 *
 * User: Meysam
 * Date: 5/2/2018
 * Time: 12:17 PM
 */

// Define required constants

if(!defined("BASE_PATH"))
    define('BASE_PATH', realpath(dirname(dirname(__FILE__))));

if(!defined("DS"))
    define('DS', DIRECTORY_SEPARATOR);

// Include required files|classes
//require_once(BASE_PATH .DS. 'vendor' .DS. 'ClassAutoloader.php');
$loader = require BASE_PATH . '/vendor/autoload.php';
//$loader->addPsr4('app\\', BASE_PATH.DS.'app'.DS);

$canvas = (new \app\widgets\CanvasWidget())
        ->position(10, 10)->add( new \app\shapes\Rectangle(30, 40) )
        ->position(15, 30)->add(new \app\shapes\Square(35))
        ->position(100, 100)->add(new \app\shapes\Ellipse(300, 200))
        ->position(1, 1)->add(new \app\shapes\Circle(300))
        ->position(5, 5)->add(new \app\shapes\Textbox(200, 100, "sample text"))

        ->draw("
-----------------------------------------------------------------------------------------------
Current Drawing
-----------------------------------------------------------------------------------------------\n
")
    ->draw("
-----------------------------------------------------------------------------------------------
");

/**
 * Also we can draw without any canvas too
 * In other word, every widget can draw as stand alone :)
 */

//$widget = new \app\base\WidgetFactory( new \app\base\BaseWidget(50, 50), new \app\shapes\Circle(300), new \app\base\renderer\CliRenderer() );

//$widget->draw();