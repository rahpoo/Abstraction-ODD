<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 5/2/2018
 * Time: 12:22 PM
 */

namespace app\shapes;

use app\base\BaseShape;

class Square extends BaseShape {

    /**
     * @var string
     */
    protected $name = "Square";

    /**
     * @var float
     */
    protected $width;

    /**
     * Square constructor.
     * @param float $width
     */
    public function __construct(float $width)
    {
        $this->width  = $width;
    }

    /**
     * @return string
     */
    public function output()
    {
        return 'size=' . $this->width;
    }

}