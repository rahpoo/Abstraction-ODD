<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 5/2/2018
 * Time: 12:23 PM
 */

namespace app\shapes;

use app\contracts\iShape;
use app\base\BaseShape;

/**
 * Class Circle
 * @package app\shapes
 */
class Circle extends BaseShape implements iShape {

    /**
     * @var string
     */
    protected $name = "Circle";
    /**
     * @var float
     */
    protected $diameter;

    /**
     * Circle constructor.
     *
     * @param float $diameter
     */
    public function __construct(float $diameter)
    {
        $this->diameter  = $diameter;
    }

    /**
     * @return string
     */
    public function output()
    {
        return "size={$this->diameter}";
    }

}