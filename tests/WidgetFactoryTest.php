<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 5/3/2018
 * Time: 6:39 PM
 */
namespace tests;

use PHPUnit\Framework\TestCase;

class WidgetFactoryTest extends TestCase
{

    public function testRectangleWidget()
    {
        $widget = new \app\base\WidgetFactory( new \app\base\BaseWidget(50, 50), new \app\shapes\Rectangle(30, 40) );

        $this->assertEquals( 'Rectangle', $widget->getShape()->getName(), 'The WidgetFactory must create a circle shape.');
    }

    public function testSquareWidget()
    {
        $widget = new \app\base\WidgetFactory( new \app\base\BaseWidget(50, 50), new \app\shapes\Square(30));

        $this->assertEquals( 'Square', $widget->getShape()->getName(), 'The WidgetFactory must create a Square shape.');
    }

    public function testEllipseWidget()
    {
        $widget = new \app\base\WidgetFactory( new \app\base\BaseWidget(50, 50), new \app\shapes\Ellipse(300, 200));

        $this->assertEquals( 'Ellipse', $widget->getShape()->getName(), 'The WidgetFactory must create a Ellipse shape.');
    }

    public function testCircleWidget()
    {
        $widget = new \app\base\WidgetFactory( new \app\base\BaseWidget(50, 50), new \app\shapes\Circle(300), new \app\base\renderer\CliRenderer() );

        $this->assertEquals( 'Circle', $widget->getShape()->getName(), 'The WidgetFactory must create a circle shape.');
    }

    public function testTextBoxWidget()
    {
        $widget = new \app\base\WidgetFactory( new \app\base\BaseWidget(50, 50), new \app\shapes\Textbox(200, 100, "sample text"), new \app\base\renderer\CliRenderer() );

        $this->assertEquals( 'Textbox', $widget->getShape()->getName(), 'The WidgetFactory must create a TextBox shape.');
    }

}
