<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 5/2/2018
 * Time: 12:23 PM
 */

namespace app\shapes;

use app\base\BaseShape;

class Textbox extends BaseShape {

    /**
     * @var string
     */
    protected $name = 'Textbox';

    /**
     * @var float
     */
    protected $width;
    /**
     * @var float
     */
    protected $height;
    /**
     * @var string
     */
    protected $text;

    /**
     * Textbox constructor.
     * @param float $width
     * @param float $height
     * @param string $text
     */
    public function __construct(float $width, float $height, string $text = "")
    {
        $this->width  = $width;
        $this->height = $height;
        $this->text   = $text;
    }

    /**
     * @return string
     */
    public function output()
    {
        return 'width='.$this->width.' height='.$this->height.' Text="'.$this->text.'"';
    }

}