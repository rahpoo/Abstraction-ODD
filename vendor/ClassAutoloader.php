<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 5/2/2018
 * Time: 12:41 PM
 */


/**
 * Autoload class files from namespaces
 *
 * @param $class
 *
 * @return bool
 * @throws Exception
 */
function class_loader($class)
{
    $class     = explode('\\', $class);
    $filename   = BASE_PATH .DS. implode(DS, $class) . '.php';
    $class     = end($class);

    if(!file_exists($filename)) {
        throw new Exception("Class " .$class. ' Not found in "' .$filename. '"');
    }

    else {
        try {
            require $filename;
        } catch (Exception $e) {
            throw new Exception("Unable to load class: $class <br/><br/>" . $e->getMessage());
        }

        return true;
    }
}

// Register autoload function in standard php libraries
spl_autoload_register('class_loader');
