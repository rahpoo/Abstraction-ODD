<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 5/2/2018
 * Time: 12:21 PM
 */

namespace app\contracts;

interface iShape {

    public function getName();
    public function output();

}