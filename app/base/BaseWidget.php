<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 5/2/2018
 * Time: 3:22 PM
 */

namespace app\base;

use app\contracts\iRenderer;
use app\contracts\iShape;
use app\contracts\iWidget;

/**
 * Class BaseWidget
 *
 * Base widget to drawing shapes by custom renderer
 *
 * @package app\base
 */
class BaseWidget implements iWidget
{

    /**
     * @var int
     */
    protected $x;

    /**
     * @var int
     */
    protected $y;

    /**
     * @var iShape
     */
    protected $shape;

    /**
     * @var null|iRenderer
     */
    protected $renderer = null;

    /**
     * BaseWidget constructor.
     *
     * @param int $x
     * @param int $y
     *
     * @param iRenderer|null $renderer
     */
    public function __construct(int $x = 0, int $y = 0, iRenderer $renderer = null)
    {
        $this->position($x, $y);
        $this->setRenderer($renderer);
    }

    /**
     * Set drawing position in current canvas
     *
     * @param int $x
     * @param int $y
     *
     * @return $this
     */
    public function position(int $x = 0, int $y = 0)
    {
        $this->setX($x);
        $this->setY($y);

        return $this;
    }

    /**
     * @param int $x
     */
    public function setX(int $x)
    {
        $this->x = $x;
    }

    /**
     * @return int
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param $y
     */
    public function setY($y)
    {
        $this->y = $y;
    }

    /**
     * @return int
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @return iShape
     */
    public function getShape()
    {
        return $this->shape;
    }

    /**
     * Set renderer for render widget
     *
     * @param iRenderer|null $renderer [default = CliRenderer]
     */
    public function setRenderer(iRenderer $renderer = null)
    {
        if(!is_a($renderer, '\app\contracts\iRenderer'))
            $this->renderer = new \app\base\renderer\CliRenderer();
        else
            $this->renderer = $renderer;
    }

    /**
     * @return iRenderer|null
     */
    public function getRenderer()
    {
        return $this->renderer;
    }

    /**
     * Render widget output using a renderer
     *
     * @param string|iShape $input
     */
    public function render($input)
    {
        $this->getRenderer()->render($input);
    }

    /**
     * Draw every widget or input string that added to this canvas
     *
     * @param string|iShape $input that draw before drawing other objects
     * @param bool $flush
     *
     * @return $this|string return $this if don't draw anything
     */
    public function draw($input, $flush = false)
    {
        if(is_string($input) && !empty($input)) {
            return $input;
        }

        elseif( is_a($input, '\app\contracts\iShape') ) {
            return $input->getName() ." ({$this->getX()},{$this->getY()}) ". $input->output();
        }

        if($flush === true)
            $this->flush();

        return $this;
    }

    /**
     * Flush cached contents
     *
     * @return void
     */
    public function flush()
    {
        unset($this->x, $this->y, $this->shape, $this->renderer);
    }

}